﻿using System;

namespace Mastermind
{
    public class Program
    {

        public static int saisie_tab_par_PC( ref int[] ChoixPc, ref int[] TabNb )
        {
            int i;
            Random aleatoire = new Random();
            //Choix des couleur a deviner par le PC
            for (i = 0; i < 4; i++)
            {
                int NbAleatoire = aleatoire.Next(6);
                ChoixPc[i] = TabNb[NbAleatoire];
            }

            return 0;
        }

        public static int saisie_nombre_coup( out int nb_coup)
        {
            string saisie;
            do
            {
                Console.WriteLine("Saisir le nombre de coup maximum possible (entre 8 et 12): ");
                saisie = Console.ReadLine();
                int.TryParse(saisie, out nb_coup);
                Console.Clear();
            } while (nb_coup < 8 || nb_coup > 12);

            return 0;
        }

        public static void affichage_des_régles(int nb_coup)
        {
            Console.WriteLine("\t \tREGLE: \n");
            Console.WriteLine("L'ordinateur a choisie 4 couleurs que vous allez devoir retrouver\n");
            Console.WriteLine("\nLes couleurs possible sont: \n  jaune \n  bleu \n  rouge \n  vert \n  blanc \n  gris \n");
            Console.WriteLine("A vous de les mettre dans le bonne ordre\n");
            Console.WriteLine("Le tableau de jeux s'affiche en dessous vous avez donc {0} coups\n", nb_coup);
        }

        public static int affichage_du_plateau_remplie_de_blanc(int nb_coup, ref int[,] plateau_jeux )
        {
            int increment1,increment2;
            
            //On affiche le tableau de jeux remplie de blanc
            for (increment1 = 0; increment1 < nb_coup; increment1++)
            {
                for (increment2 = 0; increment2 < 4; increment2++)
                {
                    plateau_jeux[increment1,increment2] = 7;
                    Console.BackgroundColor = (ConsoleColor) (plateau_jeux[increment1,increment2]);
                    Console.ForegroundColor = ConsoleColor.Black;
                    
                    if (increment2 < 4)
                    {
                        Console.Write(" - "); //écrit sens \n
                        
                    }
                }
                Console.WriteLine(""); //écrit sur une ligne et fait un \n a la fin
                              
            }
            
            Console.ResetColor();

            Console.ReadLine();
            Console.Clear();
            return 0;

        }

        public static int affichage_du_plateau(int nb_coup,ref int[,] plateau_jeux)
        {
            int increment1, increment2;
            //On affiche le plateau de jeux
            for (increment1 = 0; increment1 < nb_coup; increment1++)
            {
                for (increment2 = 0; increment2 < 4; increment2++)
                {

                    Console.BackgroundColor = (ConsoleColor) (plateau_jeux[increment1, increment2]);
                    Console.ForegroundColor = ConsoleColor.Black;

                    if (increment2 < 4)
                    {
                        Console.Write(" - ");
                    }

                }
                Console.WriteLine("");

            }

            Console.ResetColor();


            return 0;
        }

        public static int affichage_regle_couleur(out string CouleurSaisie)
        {
            //Affichage des infos
            Console.WriteLine("Le tableau de jeux est afficher au dessus \n");
            Console.WriteLine("Les couleurs sont a rentrer sous la forme couleurcouleur2couleur3couleur4\n");
            Console.WriteLine("Les couleurs sont:\n j pour jaune\n b pour bleu\n r pour rouge\n v pour vert\n B pour blanc\n g pour gris\n");
            Console.WriteLine("\n Par exemple jbrv\n");
            Console.WriteLine("Rentrer les couleurs:\n");

            CouleurSaisie = Console.ReadLine();
            Console.Clear();

            return 0;
        }

        public static int changement_lettre_en_nombre(ref string CouleurSaisie, int[] TabNb, ref int[] TabSortieCouleur )
        {
            //Changement des lettres saisie par l'utilisateur en nombre que l'on pourra afficher.
            int ChangeCase = 0;            
            char[] TabLettre = { 'j', 'b', 'r', 'v', 'B', 'g' };
            int increment,increment2;

            for (increment = 0; increment < 4; increment++)
            {
                //printf("%c\n",couleur_saisie[i]);
                for (increment2 = 0; increment2 < 6; increment2++)
                {
                    if (CouleurSaisie[increment] == TabLettre[increment2])
                    {
                        TabSortieCouleur[ChangeCase] = TabNb[increment2];
                        ChangeCase = ChangeCase + 1;
                    }
                }

            }

            return 0;
        }

        public static int mise_des_couleur_dans_plateau_de_jeux(ref int[] TabSortieCouleur, ref int[,] plateau_jeux, ref int ChangeLigne )
        {
            int increment;           
            for (increment = 0; increment < 4; increment++)
            {
                plateau_jeux[ChangeLigne,increment] = TabSortieCouleur[increment];

            }
            ChangeLigne = ChangeLigne + 1;

            return 0;
        }

        public static int verification_blanc_et_noir(ref int[] TabSortieCouleur, ref int[] choixPC, ref int[] TabVerif, ref int noir,ref int blanc  )
        {
            //Verification pour les blanc et noir

            int check,increment;
            for (increment = 0; increment < 4; increment++)
            {
                if (TabSortieCouleur[increment] == choixPC[increment])
                {
                    noir++;
                    TabVerif[increment]++;
                }
                else
                {
                    check = 0;
                    int j = 0;

                    do
                    {
                        if (check == 0 && (TabSortieCouleur[increment] == choixPC[j] && TabVerif[increment] == 0))
                        {
                            blanc++;
                            check++;
                        }
                        j++;
                    } while (check == 0 && j < 4);
                }
            }


            return 0;

        }

        public static int affichage_final(ref int noir,ref int[] ChoixPc)
        {
            int increment;
            //Affichage du final
            if (noir == 4)
            {
                Console.WriteLine("Bravos vous avez gagne!!\n");
                for (increment = 0; increment < 4; increment++)
                {
                    Console.BackgroundColor = (ConsoleColor) (ChoixPc[increment]);
                    Console.Write(" - ");
                }
                Console.ResetColor();
                return 0;
            }
            else
            {
                Console.WriteLine("Vous avez perdu la bonne reponse etait:\n");
                for (increment = 0; increment < 4; increment++)
                {
                    Console.BackgroundColor = (ConsoleColor) (ChoixPc[increment]);
                    Console.Write(" - ");
                }
                Console.ResetColor();
                return 0;
            }

            
        }



        static void Main( string[] args )
        {
            int i;
            int[] ChoixPc = new int[4];
            int[] TabNb = { 6, 1, 4, 2, 7, 8 };
            int Nb_coup;
            int Essai=1;
            int noir = 0, blanc = 0;
            string CouleurSaisie;
            int[] TabSortieCouleur = new int[4];
            int ChangeLigne = 0;
            int[] TabVerif = { 0, 0, 0, 0 };

            //Choix des couleur a deviner par le PC

            saisie_tab_par_PC(ref ChoixPc, ref TabNb);


            // Les couleurs sont afficher (pour le test)

            /*for (i = 0; i < 4; i++)
            {
                Console.BackgroundColor = (ConsoleColor) (ChoixPc[i]);  //textcolor(choixPC[i] * 16);
                Console.WriteLine("   \n");
                Console.ResetColor();
            }*/

            saisie_nombre_coup(out Nb_coup);

            affichage_des_régles(Nb_coup);

            int[,] plateau_jeux = new int[Nb_coup, 4];

            affichage_du_plateau_remplie_de_blanc(Nb_coup, ref plateau_jeux);


            //Début du jeux

            while (Essai<=Nb_coup && noir!=4)
            {
                noir = 0;
                blanc = 0;

                affichage_regle_couleur(out CouleurSaisie);

                changement_lettre_en_nombre(ref CouleurSaisie, TabNb, ref TabSortieCouleur);

                mise_des_couleur_dans_plateau_de_jeux(ref TabSortieCouleur, ref plateau_jeux, ref ChangeLigne);

                verification_blanc_et_noir(ref TabSortieCouleur, ref ChoixPc, ref TabVerif, ref noir, ref blanc);

                affichage_du_plateau(Nb_coup, ref plateau_jeux);

                //On affiche le nombre de blanc et de noir.

                Console.Write("\n{0} est le nombre de pions blanc (Pions de bonne couleur mais mal placee)", blanc);
                Console.Write("\n{0} est le nombre de pions noir (Pions de bonne couleur et bien placee)\n", noir);

                Essai = Essai + 1;

            }

            affichage_final(ref noir, ref ChoixPc);



            //Console.ForegroundColor = (ConsoleColor) (TabNb[5]); //Permet d'afficher une couleur par rapport a une valeur
            //Console.WriteLine("White on blue.");

            Console.ResetColor();
            System.Threading.Thread.Sleep(4000);
        }

        
    }
}
