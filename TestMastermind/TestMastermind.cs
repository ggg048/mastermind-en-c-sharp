﻿using System;
using Mastermind;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestMastermind
{
    [TestClass]
    public class TestMastermind
    {
        [TestMethod]
        public void TestChoixPC()
        {
            int[] ChoixPc = new int[4];
            int[] TabNb = { 6, 1, 4, 2, 7, 8 };
            Assert.AreEqual(0, Program.saisie_tab_par_PC(ref ChoixPc, ref TabNb));
        }

        /*[TestMethod]
        public void TestSaisieNbCoup()
        {
            int nb_coup;
            Assert.AreEqual(0, Program.saisie_nombre_coup(out nb_coup , "8"));
        }*/

        [TestMethod]
        public void TestAffichageTableaurempliedeblanc()
        {
            int[,] plateau_jeux = new int[4, 4];

            Assert.AreEqual(0, Program.affichage_du_plateau_remplie_de_blanc(4, ref plateau_jeux));
        }

        public void Test_affichage_du_plateau()
        {
            int[,] plateau_jeux = new int[4, 4];

            Assert.AreEqual(0, Program.affichage_du_plateau(8, ref plateau_jeux));
        }

        [TestMethod]
        public void Testchangement_lettre_en_nombre()
        {
            int[] TabNb = { 6, 1, 4, 2, 7, 8 };
            string CouleurSaisie = "jbrv";
            int[] TabSortieCouleur = new int[4];

            Assert.AreEqual(0, Program.changement_lettre_en_nombre(ref CouleurSaisie, TabNb, ref TabSortieCouleur));
        }

        [TestMethod]

        public void Test_mise_des_couleur_dans_plateau_de_jeux()
        {
            int[] TabsortieCouleur = new int[4];
            int[,] Tableau_jeux = new int[4, 5]; //ligne/colonnes
            int changeligne = 0;
            Assert.AreEqual(0, Program.mise_des_couleur_dans_plateau_de_jeux(ref TabsortieCouleur, ref Tableau_jeux, ref changeligne));
        }

        [TestMethod]
        public void Test_verification_blanc_et_noir()
        {
            int[] TabSortieCouleur = new int[4];
            int[] ChoixPc = new int[4];
            int[] Tabverif = { 0, 0, 0, 0 };
            int noir = 0, blanc = 0;
            Assert.AreEqual(0, Program.verification_blanc_et_noir(ref TabSortieCouleur, ref ChoixPc, ref Tabverif, ref noir, ref blanc));
        }

        [TestMethod]
        public void Test_affichage_final()
        {
            int noir = 4;
            int[] ChoixPc = { 6, 4, 1, 1 };
            Assert.AreEqual(0, Program.affichage_final(ref noir, ref ChoixPc));
            noir = 1;
            Assert.AreEqual(0, Program.affichage_final(ref noir, ref ChoixPc));
        }




    }
}
